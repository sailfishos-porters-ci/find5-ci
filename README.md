# Sailfish OS on Oppo Find 5 (find5)

![SailfishOS](https://sailfishos.org/wp-content/themes/sailfishos/icons/apple-touch-icon-120x120.png)

## Current Build
2.2.1.18 - [![pipeline status](https://gitlab.com/sailfishos-porters-ci/find5-ci/badges/master/pipeline.svg)](https://gitlab.com/sailfishos-porters-ci/find5-ci/commits/master)



